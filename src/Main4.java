import java.util.Scanner;

/**
 * Created by s1389950 on 1/30/2017.
 */

/* program finds the points on a graph where a line passes the x-axis
   based on line equations y = mx + b
 */
public class Main4
{
    public static double convertToCelsius(double fahrenheitTemperature)
    {
        return (fahrenheitTemperature - 32) * 5.0 /9.0;
    }


    public static double convertToFahrenheit(double celsiusTemperature)
    {
        //declare
        double fahrenheit = 0.0;

        //calculations
        fahrenheit = celsiusTemperature * 9 / 5 + 32;

        return fahrenheit;

    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void main(String[] args)
    {

        // declare and initialise all variables
        Scanner scannerIn= new Scanner(System.in);

        double fahrenheit = 0.0;
        double celsius = 0;
        double kelvin = 0.0;
        double temperatureSum = 0.0;
        double temperatureAverage = 0.0;

        String keepGoing = "y";
        int count;
        double times = 0.0;



        //input
        /*
        System.out.print("How many temperature to process: ");
        count = scannerIn.nextInt();

        //forward loop the counts the celsius temperature that has been converted from fahrenheit.
        for (int i = 0; i<count; i++) {
            //temp input
            System.out.print("Enter in a fahrenheit temperature: ");
            fahrenheit = scannerIn.nextDouble();

            //calculations
            celsius = convertToCelsius(fahrenheit);
            temperatureSum += celsius;

        }

        */

        //do while loop that counts the fahrenheit temperature after its been converted from celsius.
        do
        {
            //temp input
            System.out.print("Enter in a celsius temperature: ");
            celsius = scannerIn.nextDouble();

            //calculations
            fahrenheit = convertToFahrenheit(celsius);
            temperatureSum += fahrenheit;

            System.out.println("Do you wish to continue (y/n)?");
            keepGoing = scannerIn.next();

            times += 1.0;

        }
        while (keepGoing.toUpperCase().equals ("Y") );

        //does calculations for average temperature
        temperatureAverage = temperatureSum / times;
        System.out.println ("The average celsius temperature is: " + temperatureAverage);

        //output
        System.out.println ("Temperature in fahrenheit: " + fahrenheit);


    }

}
