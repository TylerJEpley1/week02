//note double click project to explore window, or use alt 1
/* In class programming

*/

import java.util.Scanner; // imports just files used for scanner from external library.

//import java.util.*; would pull all items from library.


public class Main
{

    public static void main(String[] args)
    {
	    // declare initialize all variables

        Scanner scannerIn = new Scanner(System.in);
        // scanner acts a method/function


        int temperature;        //initialize
        temperature = 42;       //declare

        //hard coding variables is boring!

        double fahrenheit = 32.0; //initialize and declaration
        double x2fahrenheit = 0.0; //placeholder value


        //get input
        System.out.print("Enter in today's temperature: ");
        fahrenheit = scannerIn.nextDouble();


        //perform calculations
        // x2fahrenheit *= 2.0;
        x2fahrenheit = fahrenheit * 2.0;

        //display output

        //bad form, use math in calculations
        //System.out.println("Twice the fahrenheit value is " + (fahrenheit *2) );


        System.out.println("Twice the fahrenheit value is " + (x2fahrenheit) );


    }
}
