import java.util.Scanner;

/**
 * Created by s1389950 on 1/30/2017.
 */

/* program finds the points on a graph where a line passes the x-axis
   based on line equations y = mx + b
 */
public class Main3
{
    public static int convertToFahrenheit(int celsiusTemperature)
    {
        //declare
        int fahrenheit = 0;

        //calculations
        fahrenheit = celsiusTemperature * 9 / 5 + 32;

        return fahrenheit;

    }

    public static void main(String[] args)
    {

        // declare and initialise all variables
        Scanner scannerIn= new Scanner(System.in);
        double fahrenheit = 0.0;
        int celsius = 0;
        double kelvin = 0.0;


        //input
        System.out.print ("Enter a celsius temperature: ");
        celsius = scannerIn.nextInt();;

        //calculations
        fahrenheit = convertToFahrenheit(celsius);;

        //output
        System.out.print ("Temperature in fahrenheit: " + fahrenheit);


    }

}
