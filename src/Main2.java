import java.util.Scanner;

/**
 * Created by s1389950 on 1/30/2017.
 */

/* program finds the points on a graph where a line passes the x-axis
    used on line equations y = mx + b
 */
public class Main2
{
    public static void main(String[] args)
    {
        //formula used for quadratic formula
        //
        //  X1 = , X2 =
        //                ______________________
        // -b   +/- \/ b^2 - 4 a c
        // ---------------------------------
        //              2a

        // declare and initialise all variables
        Scanner scannerIn= new Scanner(System.in);

        double a = 0.0;
        double b = 0.0;
        double c = 0.0;

        double x1 = 0.0;
        double x2 = 0.0;


        //input
        System.out.print ("Enter a value in for [a]: ");
        a = scannerIn.nextDouble();

        System.out.print ("Enter a value in for [b]: ");
        b = scannerIn.nextDouble();

        System.out.print ("Enter a value in for [c]: ");
        c = scannerIn.nextDouble();


        //calculations

        //error check data
        if (a == 0.0)
        {
            System.out.println("This equation has no solution.");
        }


        //doing actual math
        else
        {
            //positive square
            x1 = ( -b + Math.sqrt(b * b - (4.0 * a * c) ) ) / (2.0*a) ;

            //negative square
            x2 = ( -b - Math.sqrt(b * b - (4.0 * a * c) ) ) / (2.0*a) ;

            //output
            System.out.println("x1 is : " + x1);
            System.out.println("x2 is : " + x2);

        }


    }

}
